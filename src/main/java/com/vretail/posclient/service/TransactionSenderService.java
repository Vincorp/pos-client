package com.vretail.posclient.service;

import com.vretail.posclient.config.VEnv;
import com.vretail.poscommon.model.Person;
import com.vretail.poscommon.util.VLogger;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created on 1/26/17.
 *
 * @author felixsoewito
 */
@Service
public class TransactionSenderService {

    private final RabbitTemplate rabbitTemplate;
    private final Queue queue;

    @Autowired
    public TransactionSenderService(RabbitTemplate rabbitTemplate, Queue queue) {
        this.rabbitTemplate = rabbitTemplate;
        this.queue = queue;
    }

    public void sendMessage(String name, int age) {
        rabbitTemplate.convertAndSend(queue.getName(), new Person(name, age));
        VLogger.info(VEnv.V_DOMAIN.value, new Object(), "send [x] ->" + name);
    }
}
