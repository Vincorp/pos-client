package com.vretail.posclient.bean.rabbit;

import com.vretail.posclient.config.VEnv;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created on 1/26/17.
 *
 * @author felixsoewito
 */
@Configuration
public class RabbitBean {

    private static final String TOPIC_NAME = VEnv.RABBIT_TOPIC.value;
    private static final String QUEUE_NAME = VEnv.RABBIT_QUEUE.value;

    @Bean
    public Queue transactionQueue() {
        return new Queue(QUEUE_NAME, true, false, false);
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(TOPIC_NAME);
    }

    @Bean
    Binding binding(Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(QUEUE_NAME);
    }

    @Bean
    ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory(VEnv.RABBIT_HOST.value);
        connectionFactory.setUsername(VEnv.RABBIT_USER.value);
        connectionFactory.setPassword(VEnv.RABBIT_PASSWORD.value);
        connectionFactory.setVirtualHost(VEnv.RABBIT_VHOST.value);
        return connectionFactory;
    }

    @Bean
    RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory){
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setRoutingKey(QUEUE_NAME);
        template.setQueue(QUEUE_NAME);
        return template;
    }

    /**
     * @return the admin bean that can declare queues etc.
     */
    @Bean
    public AmqpAdmin amqpAdmin(ConnectionFactory connectionFactory) {
        return new RabbitAdmin(connectionFactory);
    }
}
