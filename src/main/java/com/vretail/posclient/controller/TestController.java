package com.vretail.posclient.controller;

import com.vretail.posclient.service.TransactionSenderService;
import com.vretail.poscommon.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created on 1/26/17.
 *
 * @author felixsoewito
 */
@RestController
@RequestMapping(path = "test")
public class TestController {

    private final TransactionSenderService senderService;

    @Autowired
    public TestController(TransactionSenderService senderService) {
        this.senderService = senderService;
    }

    @RequestMapping(
            path = "send",
            method = RequestMethod.POST
    )
    public String send(@RequestBody Person person) {
        senderService.sendMessage(person.getName(), person.getAge());
        return "OK";
    }
}
