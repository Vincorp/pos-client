#!/usr/bin/env bash
export V_DOMAIN="POS-CLIENT"
export RABBIT_QUEUE="vretail-transaction"
export RABBIT_TOPIC="vretail-transaction"
export RABBIT_USER="vretail"
export RABBIT_PASSWORD="vretail"
export RABBIT_VHOST="vretail"
export RABBIT_HOST="192.168.43.18"

mvn clean install exec:java -Dexec.args="--logging.path=/Users/felixsoewito/log/vretail/client --server.port=9000"